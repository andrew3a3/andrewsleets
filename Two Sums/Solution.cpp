#include <iostream>
using namespace std;

void helper(int arr[], int target);

int main() {
    int arr[4] = {2, 7, 11, 15};
    int target;
    cout << "Enter a target number to see what indices equal it, press any non-numeric key at anytime to quit" << endl;
    while(true) {
        cout << "Please enter a target number:";

        if(!(cin >> target))
            break;
        
        helper(arr, target);
    }

    cout << "Program exited" << endl;
}

/**Helper method interates the input 
 * array to find index combinattions that equal input target and prints results**/
void helper(int arr[], int target) {
    for(int i = 0; i < sizeof(arr); i++) {
        for(int j = i + 1; j < sizeof(arr); j++) {
            if((arr[i] + arr[j]) == target) {
                cout << "Array target indices are [" << i << ", "
                 << j << "]"<< endl;
                 return;
            }
        }
    }

    cout << "No such combination of indices exist in array to match target." << endl;
}
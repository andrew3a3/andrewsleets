#include <string.h>
#include <string>
#include <iostream>
#include <algorithm>
#include <vector>       
using namespace std;

void lengthOfLongestSubstring(string s);
bool noRepeat(string s);

int main() {
    string temp;
    temp = "abcabcbb";
    lengthOfLongestSubstring(temp);

    temp = "bbbbb";
    lengthOfLongestSubstring(temp);

    temp = "pwwkew";
    lengthOfLongestSubstring(temp);

    temp = "pwkewrttuyo";
    lengthOfLongestSubstring(temp);
}


void lengthOfLongestSubstring(string s) {
    int size = s.length(), longest = 0;
    string sub;

    for(int i = 0; i < size; i++) {
        for(int j = i + 1; j <= size; j++) {
            string temp = s.substr(i, j);
            //analyze substring
            if(noRepeat(temp)) {
                if(temp.length() > longest) {
                    longest = temp.length();
                    sub = temp;
                }
            }
        }
    }

    cout << "Longest substring length is " << sub << 
        " of length " << longest << endl;
}

bool noRepeat(string s) {
    //Create vector to track characters
    vector<char> chars;
    for(int i = 0; i < s.length(); i++) {
        //analyze char at index
        char curr = s[i];
        //check if list already contains char
        if(find(chars.begin(), chars.end(), curr) != chars.end())
            return false;
        
        //add if char not present
        chars.push_back(curr);
    }

    return true;
}
#include <string>
#include <iostream>
using namespace std;
 
   
class LinkedList {   
    struct Node {
        int val;
        Node *next;
    };
    Node* node;

public:
    //Creates a new linked list and mapping its parts to nodes
    LinkedList(int number) {
        int remainder, reversed;
        while(number != 0) {
            remainder = number % 10;
            this->insert(remainder);
            reversed = reversed * 10 + remainder;
            //further decrease number
            number /= 10;
        }
    }

    //Prints the linked lists mappings
    void print() {
        Node *temp = this->node;
      
        cout << "List mapping is: ";
        while(temp != NULL) {
            cout << "->" << temp->val;
            temp = temp->next;
        }

        cout << endl;
    }

    //inserts nodes into linked list
    void insert(int val) {
        Node* temp = new Node();
        temp->val = val;

        if(this->node == NULL) {
            this->node = temp;
        }
        else {
            Node* curr = this->node;
            while(curr->next != NULL) {
                curr = curr->next;
            }

            curr->next = temp;
        }
    }

    //Adds to sum of the list to its original number and returns it
    int invertList() {
        Node* curr = this->node;
        int number = 0, factor = 1;
        while(curr != NULL) {
            number += (curr->val * factor);
            factor *= 10;
            curr = curr->next;
        }

        return number;
    }
};


int main() {
    int x = 342;
    int y = 465;

    cout << "list 1 number is " << x << ", list 2 number is " << y << endl;
    //List 1
    LinkedList *list1 = new LinkedList(x);
    list1->print();

    //List 2
    LinkedList *list2 = new LinkedList(y);
    list2->print();

    //Combined List
    int sum = list1->invertList() + list2->invertList();

    cout << "Combined list sum is " << sum << endl;
    LinkedList *combinedList = new LinkedList(sum);
    combinedList->print();

}
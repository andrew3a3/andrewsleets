# AndrewsLeets

Each folder represents a different Leet Code problem. Within each folder there are 3 sperate file. **The code, problem and thought process.** The code is the solution written in C++. The Problem is the original problem presented on Leet Codes, the thought-process is a write-up
of how I thought about and approached the problem